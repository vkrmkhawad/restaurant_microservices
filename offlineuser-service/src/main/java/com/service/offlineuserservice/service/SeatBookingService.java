package com.service.offlineuserservice.service;

import java.util.Date;

import org.springframework.stereotype.Service;

@Service
public interface SeatBookingService {

	String bookSeats(int totalSeats, Date bookingDate, String userName);
}
