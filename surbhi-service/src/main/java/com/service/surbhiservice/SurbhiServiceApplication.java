package com.service.surbhiservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class SurbhiServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SurbhiServiceApplication.class, args);
	}

}
