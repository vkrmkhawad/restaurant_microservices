package com.service.userservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.service.userservice.entity.Item;

@Repository
public interface ItemDetailsRepository extends JpaRepository<Item, Integer> {

}
